Simple TOTP backup file convertor
=================================

Currently handles only Gnome Auth TOTP generator to Aegis conversion

Usage:
`authenticator-backup-convert input.json -o output.json`

Build:
`cargo build --release`