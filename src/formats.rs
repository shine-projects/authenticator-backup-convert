use crate::formats::AegisItemInfo::HOTP;
use serde::{Deserialize, Serialize};

type Secret = String;
type Algorithm = String;
type Digits = u8;
type Type = String;
type Period = u8;
type Counter = u8;
type Name = String;
type Service = String;

#[derive(Debug)]
enum AuthType {
    TOTP,
    HOTP,
    STEAM,
}

const AEGIS_VERSION: u8 = 1;
const AEGIS_DB_VERSION: u8 = 1;

#[derive(Debug)]
pub struct GenericAuthItem {
    secret: Secret,
    name: Name,
    issuer: Service,
    auth_type: AuthType,
    algorithm: Algorithm,
    digits: Digits,
    period: Period,
}

#[derive(Debug)]
pub struct GenericAuthItems(Vec<GenericAuthItem>);

impl From<GnomeAuthenticator> for GenericAuthItems {
    fn from(authenticator: GnomeAuthenticator) -> Self {
        GenericAuthItems(
            authenticator
                .into_iter()
                .map(|item| {
                    let issuer = item.tags[0].clone();
                    GenericAuthItem {
                        secret: item.secret,
                        name: item.label,
                        issuer,
                        auth_type: AuthType::TOTP,
                        algorithm: item.algorithm,
                        digits: item.digits,
                        period: item.period,
                    }
                })
                .collect(),
        )
    }
}

#[derive(Serialize, Deserialize)]
pub struct GnomeAuthenticatorItem {
    pub secret: Secret,
    pub label: String,
    pub period: Period,
    pub digits: Digits,
    #[serde(rename = "type")]
    pub auth_type: Type,
    pub algorithm: Algorithm,
    pub thumbnail: String,
    pub tags: Vec<Service>,
}

pub type GnomeAuthenticator = Vec<GnomeAuthenticatorItem>;

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
enum AegisItemInfo {
    HOTP {
        secret: Secret,
        algo: Algorithm,
        digits: Digits,
        counter: Counter,
    },
    TOTP {
        secret: Secret,
        algo: Algorithm,
        digits: Digits,
        period: Period,
    },
}

#[derive(Serialize, Deserialize)]
pub struct AegisItem {
    #[serde(rename = "type")]
    auth_type: Type,
    name: String,
    issuer: String,
    icon: Option<String>,
    info: AegisItemInfo,
}

#[derive(Serialize, Deserialize)]
pub struct AegisDb {
    version: u8,
    entries: Vec<AegisItem>,
}

#[derive(Serialize, Deserialize)]
pub struct AegisHeaderParams {
    nonce: String,
    tag: String,
}

#[derive(Serialize, Deserialize)]
pub struct AegisHeader {
    slots: Option<u8>,
    params: Option<AegisHeaderParams>,
}

#[derive(Serialize, Deserialize)]
pub struct Aegis {
    version: u8,
    header: AegisHeader,
    db: AegisDb,
}

impl From<GenericAuthItems> for Aegis {
    fn from(auth_items: GenericAuthItems) -> Self {
        let entries = auth_items
            .0
            .into_iter()
            .map(|item| {
                let (info, auth_type) = match item.auth_type {
                    AuthType::TOTP => (
                        AegisItemInfo::TOTP {
                            secret: item.secret,
                            algo: item.algorithm,
                            digits: item.digits,
                            period: item.period,
                        },
                        "totp",
                    ),
                    AuthType::STEAM => (
                        AegisItemInfo::TOTP {
                            secret: item.secret,
                            algo: item.algorithm,
                            digits: item.digits,
                            period: item.period,
                        },
                        "steam",
                    ),
                    AuthType::HOTP => (
                        AegisItemInfo::HOTP {
                            secret: item.secret,
                            algo: item.algorithm,
                            digits: item.digits,
                            counter: item.period,
                        },
                        "hotp",
                    ),
                };

                AegisItem {
                    auth_type: String::from(auth_type),
                    name: item.name,
                    issuer: item.issuer,
                    icon: None,
                    info,
                }
            })
            .collect();

        let db = AegisDb {
            version: AEGIS_DB_VERSION,
            entries,
        };

        let header = AegisHeader {
            slots: None,
            params: None,
        };

        Aegis {
            version: AEGIS_VERSION,
            header,
            db,
        }
    }
}
