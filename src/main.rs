use clap::Clap;
use std::fs::File;
use std::io::{BufReader, Write};
mod formats;

/// Converts backup format of authenticator file from GNOME Authenticator  to Aegis
#[derive(Clap)]
#[clap(version = "1.0", author = "Kateřina Churanová <katerina@churanova.eu>")]
struct Opts {
    /// Input file
    input: String,
    /// Output file. Empty for STDOUT
    #[clap(short, long)]
    output: Option<String>,
}

fn main() {
    let opts = Opts::parse();
    let file = File::open(opts.input).expect("could not open file");
    let reader = BufReader::new(file);
    let input: formats::GnomeAuthenticator =
        serde_json::from_reader(reader).expect("Can't parse the input");

    let generic_format: formats::GenericAuthItems = input.into();
    let aegis: formats::Aegis = generic_format.into();

    let output_json = serde_json::to_string_pretty(&aegis).unwrap();
    if let Some(output_file) = opts.output {
        let mut file = File::create(&output_file)
            .expect(format!("Can't open file {} for writing", output_file).as_str());
        file.write_all(output_json.as_bytes())
            .expect("Couldn't write to file");
    } else {
        std::io::stdout()
            .write_all(output_json.as_bytes())
            .expect("Failed to output to STDOUT");
    };
}
